<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\StudentsGroups;

class Groups extends Controller {
	public function index() {
		$groups = StudentsGroups::all();

		return view('students.index', ['groups' => $groups]);
	}
}
