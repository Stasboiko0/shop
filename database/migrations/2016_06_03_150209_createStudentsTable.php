<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('students', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('students_groups_id');
			$table->string('name_student');
			$table->integer('phone_student');
			$table->integer('age_student');
			$table->string('mail_student');
			$table->string('address_student');
			$table->double('gpa_student',15, 8);
			$table->double('attendance_student',15, 8);
			$table->string('last_visit_student');
			$table->string('picture');
            $table->timestamps();
        });

		Schema::create('students_groups', function (Blueprint $table) {
			$table->increments('id');
			$table->string('name_groups');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::drop('students');

		Schema::drop('students_groups');
	}
}
