<?php

use Illuminate\Database\Seeder;

class StudentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i=0;$i<10;$i++)
        {
            DB::table('students_groups')
              ->insert([

                  'name_groups' => str_random(10),

              ]);
        };
    }
}
